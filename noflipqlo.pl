#!/usr/bin/perl
use OpenGL qw(:all);
use OpenGL::FTGL ':all';
use OpenGL::XScreenSaver;
use POSIX qw(strftime), qw(floor);
use Time::HiRes 'sleep', 'time';
 
OpenGL::XScreenSaver::init();
# GetOptions(...); # parse your own options, if any

OpenGL::XScreenSaver::start();

($width, $height) = OpenGL::XScreenSaver::dimensions();

$fontFile = "/usr/share/fonts/truetype/droid/DroidSans-Bold.ttf";
my $font = ftglCreateTextureFont($fontFile) or die $!;
ftglSetFontFaceSize($font, $height/2);

$aspectRatio = $width/$height;
#print "Aspect ratio = $aspectRatio\n";
glScalef(1/$aspectRatio * 2, 2, 1);

#/* CALCULATE BACKGROUND COORDINATES */
#hourBackground.y = 0.2 * customHeight;
$hourBackgroundY = 0.2;
#hourBackground.x = 0.5 * (customWidth - ((0.031)*customWidth) - (1.2 * customHeight));
$hourBackgroundX = 0.5 * ($aspectRatio - ((0.031)*$aspectRatio) - (1.2));
#hourBackground.w = customHeight * 0.6;
#hourBackground.h = hourBackground.w;
$backgroundSize = 0.6;
#ifdef DEBUG
    #printf(" Hour x coordinate %d\n Hour y coordinate %d\n", hourBackground.y, hourBackground.x);
#endif
#spacing = 0.031 * customWidth;
$spacing = 0.031 * $aspectRatio;
    #minBackground.x = hourBackground.x + (0.6*customHeight) + spacing;
$minBackgroundX = $hourBackgroundX + $backgroundSize + $spacing;
    #minBackground.y = hourBackground.y;
$minBackgroundY = $hourBackgroundY;
#ifdef DEBUG
#printf (" Minute x coordinate %d\n Minute y coordinate %d\n", minBackground.x, minBackground.y);
#endif

$backgroundCornerRadius = $backgroundSize / 6;
$fontSize = 0.375;

#glEnable( GL_BLEND | GL_LINE_SMOOTH );

glClearColor(0.0, 0.0, 0.0, 1.0);
# Move (0, 0) to top-left corner.
glTranslatef(-0.5 - ($aspectRatio-1)/2, -0.5, 0);

sub draw_rounded_rectangle {
    my($x, $y, $w, $h, $r) = @_;

    glPushMatrix();
        glTranslatef($x,$y,0);

        glBegin(GL_QUADS);
            # Full height rectangle
            glVertex2f($r, 0);
            glVertex2f($w - $r, 0);
            glVertex2f($w - $r, $h);
            glVertex2f($r, $h);
            # Full width rectangle
            glVertex2f(0, $r);
            glVertex2f(0, $h - $r);
            glVertex2f($w, $h - $r);
            glVertex2f($w, $r);
        glEnd();

        $quadratic = gluNewQuadric();
        gluQuadricNormals($quadratic, GLU_SMOOTH);
        glPushMatrix();
            glTranslatef($r,$r,0);
            gluDisk($quadratic,0,$r,100,100);
        glPopMatrix();
        glPushMatrix();
            glTranslatef($w-$r,$r,0);
            gluDisk($quadratic,0,$r,100,100);
        glPopMatrix();
        glPushMatrix();
            glTranslatef($w-$r,$h-$r,0);
            gluDisk($quadratic,0,$r,100,100);
        glPopMatrix();
        glPushMatrix();
            glTranslatef($r,$h-$r,0);
            gluDisk($quadratic,0,$r,100,100);
        glPopMatrix();
    glPopMatrix();
}

sub draw_text {
    my($str, $scale, $x, $y, $w, $h) = @_;

    @boundingBox = ftglGetFontBBox($font, $str);
    $textWidth = $boundingBox[3] - $boundingBox[0];
    $textHeight = $boundingBox[4] - $boundingBox[1];
    $normalizedWidth = $textWidth/$textHeight;
    $centeringX = ($w - $normalizedWidth*$scale)/2;
    $centeringY = ($h - $scale)/2;
    glPushMatrix();
        # TODO Center text in box ($x, $y, $w, $h)
        glTranslatef($x, $y, 0);

        glTranslatef(0, $centeringY, 0);
        glTranslatef($centeringX, 0, 0);
        glScalef($scale, $scale, 1);

        glScalef(1/$textHeight,1/$textHeight,1);
        glTranslatef(-$boundingBox[0], -$boundingBox[1], 0);
        ftglRenderFont($font, $str);
    glPopMatrix();
}

sub draw_screen {
    my($hour, $minute) = @_;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor3ub(0x0a, 0x0a, 0x0a);
    draw_rounded_rectangle($hourBackgroundX, $hourBackgroundY, $backgroundSize, $backgroundSize, $backgroundCornerRadius);
    draw_rounded_rectangle($minBackgroundX, $minBackgroundY, $backgroundSize, $backgroundSize, $backgroundCornerRadius);

    glColor3ub(0xb7, 0xb7, 0xb7);
    draw_text($hour, $fontSize, $hourBackgroundX, $hourBackgroundY, $backgroundSize, $backgroundSize);
    draw_text($minute, $fontSize, $minBackgroundX, $minBackgroundY, $backgroundSize, $backgroundSize);
}

# Initial display
$time = time;
$hour = strftime "%H", localtime $time;
$minute = strftime "%M", localtime $time;
draw_screen($hour, $minute);
OpenGL::XScreenSaver::update();
$displayedTime = $toDisplay;

while (1) {
    $time = time;
    $hour = strftime "%H", localtime $time;
    $minute = strftime "%M", localtime $time;

    $toDisplay = $hour . $minute;
    $futureTime = $time;
    while ($toDisplay eq $displayedTime) {
        $futureTime = floor($futureTime + 1);

        $hour = strftime "%H", localtime $futureTime;
        $minute = strftime "%M", localtime $futureTime;
        $toDisplay = $hour . $minute;
    }
    draw_screen($hour, $minute);

    $toSleep = $futureTime-time;
    if ($toSleep > 0) {
        sleep $toSleep;
    }
    OpenGL::XScreenSaver::update();
    $displayedTime = $toDisplay;
    sleep 1;
}
